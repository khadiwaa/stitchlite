<?php

namespace Stitch\Vendor;

interface VendorInterface
{

    /**
     * Get a list of products from API
     * @param array $parameters Criteria for which products to retrieve
     * @return array
     */
    public function getProducts( $parameters = [] );

    /**
     * Syncs products with vendor. Updates existing products, and creates new products
     * @return boolean
     */
    public function syncProducts();

    /**
     * Creates the remote product
     * @param \Product $product
     * @return boolean
     */
    public function createProduct( \Product $product );

    /**
     * Creates the remote variant
     * @param \Variant $variant
     * @return boolean
     */
    public function createVariant( \Variant $variant );

    /**
     * Updates the remote variant
     * @param \Variant $variant
     * @return boolean
     */
    public function updateVariant( \Variant $variant );

}