<?php
namespace Stitch\Vendor;

class Shopify extends VendorAbstract
{
    const PATH_TO_PRODUCTS = '/admin/products';

    const PATH_TO_VARIANTS = '/admin/variants';

    protected $_vendorIdentifier = 'shopify';

    protected $_authType = self::AUTH_TYPE_BASIC;

    protected $_defaultHeaders = [];

    public function getProducts( $parameters = [] ) {
        return $this->_httpGet( self::PATH_TO_PRODUCTS . '.json' . '?' . http_build_query( $parameters ) )['products'];
    }

    public function createProduct( \Product $product ) {

        $vendor = \Vendor::where( 'identifier', '=', $this->_vendorIdentifier )->first();

        $data = [
          'product' => [
              'title' => $product->name,
          ]
        ];

        $shopifyProduct = $this->_httpPost( self::PATH_TO_PRODUCTS . '.json', json_encode( $data ) );

        if( isset( $shopifyProduct['product'] ) && isset( $shopifyProduct['product']['id'] ) ) {

            \VendorProduct::create( [ 'vendor_id' => $vendor->id, 'product_id' => $product->id, 'product_identifier' => $shopifyProduct['product']['id'] ] );

        } else {

            return false;

        }

        foreach( $product->variants() as $variant ) {

            if( !$this->createVariant( $variant ) ) {

                return false;

            }

        }

        return true;

    }

    public function updateVariant( \Variant $variant ) {

        if( $vendorVariant = $variant->getVendorVariant( $this->_vendorIdentifier ) ) {

            $data = [ 'variant' => [

                'id' => (int) $vendorVariant->variant_identifier,
                'inventory_quantity' => (int) $variant->quantity,
                'price' => (float) $variant->price,

            ] ];

            $this->_httpPut( self::PATH_TO_VARIANTS . '/' . $vendorVariant->variant_identifier . '.json', json_encode( $data ) );

            return true;

        } else {

            return false;

        }

    }

    public function createVariant( \Variant $variant ) {

        if( $vendorProduct = $variant->product()->getVendorProduct( $this->_vendorIdentifier ) ) {

            $data = [ 'variant' => [

                'option1' => $variant->name,
                'inventory_quantity' => $variant->quantity,
                'price' => $variant->price,

            ] ];

            $response = $this->_httpPut( self::PATH_TO_PRODUCTS . '/' . $vendorProduct->product_identifier. '/variants.json', json_encode( $data ) );

            if( isset( $response['variant'] ) && isset( $response['variant']['id'] ) ) {

                $vendor = \Vendor::where( 'identifier', '=', $this->_vendorIdentifier )->first();
                \VendorVariant::create( [ 'variant_identifier' => $response['variant']['id'], 'vendor_id' => $vendor->id, 'variant_id' => $variant->id ] );
                return true;

            } else {

                return false;

            }

        } else {

            return false;

        }

    }

    public function syncProducts() {

        $products = $this->getProducts();
        $skuList = [];
        $appTimezone = new \DateTimeZone( date_default_timezone_get() );
        $vendor = \Vendor::where( 'identifier', '=', $this->_vendorIdentifier )->first();

        foreach( $products as $product ) {

            // Find or Create Product
            $productObj = null;

            foreach( $product['variants'] as $variant ) {

                if( $variantObj = \Variant::where( 'sku', '=', $variant['sku'] )->first() ) {

                    $productObj = $variantObj->product();
                    break;

                }

            }

            // If variant does not exist, look for product, if that does not exist, create it
            if( empty( $productObj ) ) {

                $productObj = \Product::create( [ 'name' => $product['title'] ] );
                \VendorProduct::create( [ 'product_id' => $productObj->id, 'vendor_id' => $vendor->id, 'product_identifier' => $product['id'] ] );

            }

            // If variant does not exist locally, create it. If it does exist, update either local or remote record
            // based on which is least recent
            foreach( $product['variants'] as $variant ) {

                $skuList[] = $variant['sku'];

                if( !( $variantObj = \Variant::where( 'sku', '=', $variant['sku'] )->first() ) ) {

                    $variantObj = new \Variant( [ 'sku' => $variant['sku'], 'product_id' => $productObj->id ] );
                    $variantObj->name = $variant['option1'];
                    $variantObj->quantity = $variant['inventory_quantity'];
                    $variantObj->price = $variant['price'];
                    $variantObj->save();

                    \VendorVariant::create( [ 'variant_identifier' => $variant['id'], 'vendor_id' => $vendor->id, 'variant_id' => $variantObj->id ] );

                }  else {

                    // Check to see if variant has been synced with this vendor. If not, create vendor identifier
                    $vendorVariant = \Variant::whereHas( 'vendor_variants', function( $q ) use( $vendor ) {

                        $q->where( 'vendor_id', '=', $vendor->id );

                    });

                    if( empty( $vendorVariant ) ) {

                        $vendorVariant = \VendorVariant::create( [ 'vendor_id' => $vendor->id, 'variant_id' => $variantObj->id, 'variant_identifier' => $variant['id'] ] );

                    } else if( new \DateTime( $variant['updated_at'] ) > new \DateTime( $variantObj['updated_at'], $appTimezone ) ) {

                        $variantObj->name = $product['title'] . ' / ' . $variant['option1'];
                        $variantObj->quantity = $variant['inventory_quantity'];
                        $variantObj->price = $variant['price'];
                        $variantObj->save();

                    } else {

                        $this->updateVariant( $variantObj );

                    }

                }

            }

        }

        $missingRemoteProducts = \Product::whereHas( 'variants', function($q) use( $skuList ) {

            $q->whereNotIn( 'sku', $skuList );

        });

        foreach( $missingRemoteProducts->get() as $product ) {

            $this->createProduct( $product );

        }

        return true;
    }

}