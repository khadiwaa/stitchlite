<?php
namespace Stitch\Vendor;

class Vend extends VendorAbstract
{

    const PATH_TO_GET_PRODUCTS = '/api/products';

    protected $_vendorIdentifier = 'vend';

    protected $_apiUrl = 'bassface.vendhq.com';

    protected $_authType = self::AUTH_TYPE_BASIC;

    public function getProducts( $parameters = [ 'active' => 1 ] ) {
        return $this->_httpGet( self::PATH_TO_GET_PRODUCTS . '?' . http_build_query( $parameters ) )['products'];
    }

    public function updateProduct( \Product $product ) {

    }

    public function createProduct( \Product $product ) {

    }

    public function syncProducts() {
        $products = $this->getProducts();

        foreach( $products as $product ) {
            if( $product['sku'] == 'vend-discount' ) {
                continue;
            }
            if( !( $productObj = \Product::where( 'sku', '=', $product['sku'] )->first() ) ) {
                $productObj = new \Product;
                $productObj->sku = $product['sku'];
            }

            $productObj->name = $product['name'];
            $productObj->quantity = 0;
            foreach( $product['inventory'] as $inventory ) {
                $productObj->quantity += $inventory['count'];
            }
            $productObj->price = $product['price'];
            $productObj->save();
        }

        return true;
    }

}