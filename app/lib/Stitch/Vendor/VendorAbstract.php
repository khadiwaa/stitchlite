<?php

namespace Stitch\Vendor;
use GuzzleHttp;
use Whoops\Example\Exception;

abstract class VendorAbstract implements VendorInterface
{
    const AUTH_TYPE_BASIC = 'basic';

    /**
     * @var string
     */
    protected $_vendorIdentifier;

    /**
     * @var string
     */
    protected $_apiRequiresSSL = true;

    /**
     * @var string
     */
    protected $_apiUrl;

    /**
     * @var string
     */
    protected $_apiUsername;

    /**
     * @var string
     */
    protected $_apiPassword;

    /**
     * @var string
     */
    protected $_apiSecret;

    /**
     * Type of authentication with API. Possible value: basic
     * @var string
     */
    protected $_authType;

    /**
     * @var \GuzzleHttp\Client
     */
    private $_client;

    /**
     * @var string
     */
    private $_baseUrl;

    public function __construct( $apiUrl, $username, $password, $secret = ''  ) {
        $this->_apiUrl = $apiUrl;
        $this->_apiUsername = $username;
        $this->_apiPassword = $password;
        $this->_apiSecret = $secret;

        $this->_setupClient();
    }

    /**
     * @param $path
     * @return array
     */
    protected function _httpGet( $path ) {
        return $this->_parseResponse( $this->_client->get( $this->_baseUrl . $path, ['auth' => [ $this->_apiUsername, $this->_apiPassword ] ] ) );
    }

    /**
     * @param $path
     * @param array $data
     * @return array
     */
    protected function _httpPost( $path,  array $data ) {
        return $this->_parseResponse( $this->_client->post( $this->_baseUrl . $path, ['auth' => [ $this->_apiUsername, $this->_apiPassword ], 'body' => $data ] ) );
    }

    /**
     * @param $path
     * @param mixed $data
     * @return array
     */
    protected function _httpPut( $path, $data ) {
        return $this->_parseResponse( $this->_client->put( $this->_baseUrl . $path, ['auth' => [ $this->_apiUsername, $this->_apiPassword ], 'body' => $data ] ) );
    }

    /**
     * @param \GuzzleHttp\Message\Response $responseString
     * @return array
     */
    protected function _parseResponse( \GuzzleHttp\Message\Response $responseString ) {
        return $responseString->json();
    }

    /**
     * Sets up HTTP client
     * @throws \Exception
     */
    private function _setupClient() {
        $this->_client = new GuzzleHttp\Client();
        switch( $this->_authType ) {
            case self::AUTH_TYPE_BASIC:
                if( empty( $this->_apiUsername ) || empty( $this->_apiPassword ) ) {
                    throw new \Exception( 'Username and password must be initialized for ' . get_class( $this ) );
                }
                $this->_baseUrl = 'http' . ( $this->_apiRequiresSSL ? 's' : '' ) . '://' . $this->_apiUrl;
                break;
            default:
                throw new \Exception( 'Auth type is invalid for ' . get_class( $this ) );
        }
    }
}