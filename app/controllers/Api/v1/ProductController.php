<?php

namespace Api\v1;

use Illuminate\Routing\Controller;
use \Illuminate\Support\Facades\Response;

class ProductController extends Controller {

    public function all() {

        $products = \Product::all();

        $response = [];

        foreach( $products as $product ) {

            $response[] = $this->_prepareProductOutput( $product );

        }

        return Response::json( $response );
    }

    public function get( $id ) {

        $product = \Product::find( $id );
        return Response::json( $this->_prepareProductOutput( $product ) );

    }

    public function sync() {

        $results = [];

        foreach( Vendor::all() as $vendor ) {

            $config = Config::get( 'app.stitch.vendors.' . $vendor->identifier );
            $className = 'Stitch\Vendor\\' . ucfirst( $vendor->identifier );
            $vendorClass = new $className( $config['apiUrl'], $config['apiUsername'], $config['apiPassword'], ( isset( $config['apiSecret'] ) ? $config['apiSecret'] : '' ) );

            if( $vendorClass->syncProducts() ) {
                $results[] = "Successfully synced with {$vendor}";
            } else {
                $results[] = "There was an error syncing with {$vendor}";
            }

        }

        return Response::json( [ 'results' => $results ] );

    }

    private function _prepareProductOutput( $product ) {
        $variants = [];

        foreach( $product->variants as $variant ) {

            $variants[] = [

                'variant_id' => $variant->id,
                'name'       => $variant->name,
                'price'      => $variant->price,
                'quantity'   => $variant->quantity,
                'quantity'   => $variant->sku,

            ];
        }

        return [

            'id'       => $product->id,
            'name'     => $product->name,
            'variants' => $variants

        ];
    }
}