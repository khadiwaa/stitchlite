<?php

class VendorProduct extends Eloquent
{
    protected $fillable = [ 'vendor_id', 'product_id', 'product_identifier' ];

    public function vendor()
    {
        return $this->belongsTo( 'Vendor' );
    }

    public function product()
    {
        return $this->belongsTo( 'Product' );
    }
}
