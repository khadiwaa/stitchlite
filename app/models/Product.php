<?php

class Product extends Eloquent
{

    protected $fillable = ['name'];

    public function variants()
    {
        return $this->hasMany( 'Variant' );
    }

    public function product_variants()
    {
        return $this->hasMany( 'ProductVariant' );
    }

    /**
     * @param $vendorIdentifier
     * @return \VendorProduct|null
     */
    public function getVendorProduct( $vendorIdentifier )
    {
        $sku = $this->sku;
        $vendor = \Vendor::where( 'identifier', '=', $vendorIdentifier )->first();
        return \ProductVariant::whereHas( 'products', function( $q ) use ( $sku ) {
            $q->where( 'sku', '=', $sku );
        })->where( 'vendor_id', '=', $vendor->id )->get();
    }

}
