<?php

class Variant extends Eloquent
{

    protected $fillable = ['sku', 'product_id'];

    public function product()
    {
        return $this->belongsTo( 'Product' );
    }

    public function vendor_variants()
    {
        return $this->hasMany( 'VendorVariant' );
    }

    /**
     * @param $vendorIdentifier
     * @return \VendorVariant|null
     */
    public function getVendorVariant( $vendorIdentifier )
    {
        $sku = $this->sku;
        $vendor = \Vendor::where( 'identifier', '=', $vendorIdentifier )->first();
        return \VendorVariant::whereHas( 'variant', function( $q ) use ( $sku ) {
            $q->where( 'sku', '=', $sku );
        })->where( 'vendor_id', '=', $vendor->id )->get()->first();
    }

}
