<?php

class VendorVariant extends Eloquent
{
    protected $fillable = [ 'vendor_id', 'variant_id', 'variant_identifier' ];

    public function vendor()
    {
        return $this->belongsTo( 'Vendor' );
    }

    public function variant()
    {
        return $this->belongsTo( 'Variant' );
    }
}
