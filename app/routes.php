<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'api/v1'], function () {
    Route::get('products',      'Api\v1\ProductController@all');
    Route::get('products/{id}', 'Api\v1\ProductController@get');
    Route::post('sync',         'Api\v1\ProductController@sync');
});
