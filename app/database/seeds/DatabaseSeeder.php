<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('VendorSeeder');
	}

}

class VendorSeeder extends Seeder {

    public function run()
    {
        DB::table('vendors')->delete();

        Vendor::create( [ 'name' => 'Shopify', 'identifier' => 'shopify', 'active' => 1 ] );
        Vendor::create( [ 'name' => 'Vend', 'identifier' => 'vend', 'active' => 1 ] );

    }

}