<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class StitchVendorSync extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'stitch:vendor_sync';

    /**
     * @var \Stitch\Vendor\VendorAbstract
     */
    protected $vendorClass;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs products with the vendor.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $vendor = $this->argument( 'vendor' );
        $className = 'Stitch\Vendor\\' . ucfirst( $vendor );
        $config = Config::get( 'app.stitch.vendors.' . $vendor );
        $this->vendorClass = new $className( $config['apiUrl'], $config['apiUsername'], $config['apiPassword'], ( isset( $config['apiSecret'] ) ? $config['apiSecret'] : '' ) );
        if( $this->vendorClass->syncProducts() ) {
            $this->info( "Successfully synced with {$vendor}" );
        } else {
            $this->info( "There was an error syncing with {$vendor}"  );
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('vendor', InputArgument::REQUIRED, 'Name of vendor that should be synced'),
        );
    }

}
